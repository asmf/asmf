#ifndef ASMF_HH
#define ASMF_HH

#include <stdio.h>
#include <sstream>
#include <stdarg.h>
#include <string.h>


inline void asmf(const char* fmt, ...) {
    std::stringstream result;

    va_list argp;
    va_start(argp, fmt);
    
    for (const char* p = fmt; *p != '\0'; p++) {
        if (p[0] != '%') {
            result << *p;
            continue;
        } else if (p[1] == 'r') {
            unsigned long i = va_arg(argp, unsigned long);
            result << 'r' << i;
        } else if (p[1] == '(') {
            long i = va_arg(argp, long);
            result << i;
            while(p[1] != ')') { p++; }
        } else if ((p[1] >= '0') && (p[1] <= '9')) {
            long i = va_arg(argp, long);
            result << i;
            while(p[1] != 'i') { p++; }
        }
        p++;
    }
    result << '\n';
    unsigned char** bufp = va_arg(argp, unsigned char**);
    unsigned char* buf = *bufp;
    memcpy(*bufp, result.str().c_str(), result.str().length() + 1);
    *bufp += result.str().length();

    va_end(argp);
}

#endif
