import re
import math

__all__ = ["instantiate_wildcards", "WildcardInstantiation"]

###
### Generate concrete values for wildards
###


def int2bin(value, num_bits):
    if value >= 0:
        b = bin(value)[2:].replace("L", "")
        assert len(b) <= num_bits
        return "0" * (num_bits - len(b)) + b
    else:
        b = bin(value + (1 << num_bits))[2:].replace("L", "")
        assert len(b) <= num_bits
        return "1" * (num_bits - len(b)) + b

def int2hex(value):
    return hex(value).replace('L', '')


def defines_instantiator(wildcard, define):
    """
    Generates instantiations of user define wildcards kinds.
    """
    bitwidth = int(math.ceil(math.log(len(define), 2)))
    for regnum, regname in enumerate(define):
        yield WildcardInstantiation(regname, int2bin(regnum, bitwidth))




def _do_generate_imms(bits):
    if bits <= 4:
        yield "1100"
        yield "0011"
        yield "0101"
    else:
        half = bits / 2
        yield "1" * half + "0" * half
        for imm in _do_generate_imms(half):
            yield imm * 2

def _generate_imms(bits):
    for value in _do_generate_imms(bits):
        yield value[(len(value) - bits):]

def generate_imms(bits):
    yield "0"
    if bits > 8:
        for x in _generate_imms(8):
            yield x
    if bits > 16:
        for x in _generate_imms(16):
            yield x
    if bits > 32:
        for x in _generate_imms(32):
            yield x
    if bits > 64:
        for x in _generate_imms(64):
            yield x
    if bits > 128:
        for x in _generate_imms(128):
            yield x
    for x in _generate_imms(bits):
        yield x


def bin2sint(bin):
    if bin[0] == '1': # negative
        return int(bin, 2) - (1 << len(bin))
    else:
        return int(bin, 2)


def immediate_instantiator(wildcard, dummy):
    """
    Generates instantiations of immediate wildcards.
    """
    if "-" in wildcard:
        low, high = wildcard[:-1].split('-')
        low = int(low)
        high = int(high)
    else:
        low = 0
        high = int(wildcard[:-1])

    for imm in generate_imms(high):
        asm = str(bin2sint(imm))
        imm = imm[0] * (high - len(imm) + 1) + imm[1:] # sign extend.
        yield WildcardInstantiation(asm, imm)


def expr_instantiator(wildcard, dummy):
    values = list(eval(wildcard))
    bit_width = max(len(bin(v)) - 2 for v in values)
    for i in values:
        yield WildcardInstantiation(str(i), int2bin(i, bit_width))


def do_instantiate_wildcards(all_instantiations, accumulator, wildcards, defines):
    if wildcards != []:
        wildcard = wildcards[0]
        next_wildcards = wildcards[1:]
        wildcard_kind = wildcard[-1]
        if wildcard_kind == 'i':
            define = None
            instantiator = immediate_instantiator
        elif wildcard_kind == ')':
            define = None
            instantiator = expr_instantiator
        else:
            define = defines[wildcard_kind]
            instantiator = defines_instantiator
        for instantiation in instantiator(wildcard, define):
            next_accumulator = accumulator + [instantiation]
            do_instantiate_wildcards(all_instantiations,
                                     next_accumulator,
                                     next_wildcards,
                                     defines)
    else:
        all_instantiations.append(tuple(accumulator))


class WildcardInstantiation:
    """
    A wildcard instantiation is a Wildcard that has been given a
    concrete value.
    """

    def __init__(self, asm_value, bin_value):
        """
        asm_value: the value as representedWildcard in assembly (str).
        bin_value: the value as represented in the assembled
            binary file (0-padded str of 1 and 0).
        """
        self.asm_value = asm_value
        self.bin_value = bin_value

WILDCARD_PATTERN = re.compile(r'%((\(.*?\))|(([-0-9]+)i)|[a-zA-Z]\b)')
def instantiate_wildcards(pattern, defines):
    """
    A pattern is a assembler sequence with wildcards, e.g.,
    'mov %r, %32i'. This function returns concrete instantiations
    of such pattern by generating the cross product of the wildcards,
    that is, all combinations of values the wildcards can take.
    """
    wildcards = list(m.group(1) for m in WILDCARD_PATTERN.finditer(pattern))
    all_instantiations = []
    do_instantiate_wildcards(all_instantiations, [], wildcards, defines)
    return all_instantiations

