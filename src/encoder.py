__all__ = ["find_encoding"]


def indices_of_bits(bitstr):
    ones = []
    zeros = []
    for idx, bit in enumerate(bitstr):
        if bit == '1':
            ones.append(idx)
        elif bit == '0':
            zeros.append(idx)
        else:
            assert False
    return ones, zeros


class ExampleSet:
    def __init__(self, encoding_example, operands_examples):
        self._num_examples = 0
        options = set(idx for idx in range(len(encoding_example)))
        self._operands = [[options.copy() for _ in ex]
                          for ex in operands_examples]
        self._opcode_value = list(encoding_example)

        self._all_operands = [set() for _ in operands_examples]


    def _try_add_example(self, encoding_example, operands_examples):
        ones, zeros = indices_of_bits(encoding_example)

        next_operands = [] 
        non_operand_indices = set(range(len(encoding_example)))
        for op, ex in zip(self._operands, operands_examples):
            next_op = []
            for op_bit, ex_bit in zip(op, ex):
                if ex_bit == '1':
                    next_op_bit = op_bit.intersection(ones)
                elif ex_bit == '0':
                    next_op_bit = op_bit.intersection(zeros)
                else:
                    assert False
                if len(next_op_bit) == 0:
                    return None
                elif len(next_op_bit) == 1:
                    self._opcode_value[list(next_op_bit)[0]] = '-'
                non_operand_indices.difference_update(op_bit)
                next_op.append(next_op_bit)
            next_operands.append(next_op)

        for idx in non_operand_indices:
            ex = encoding_example[idx]
            op = self._opcode_value[idx]
            if op != '-' and op != ex:
                return None

        return next_operands


    def add_example(self, encoding_example, operands_examples):
        if len(encoding_example) != len(self._opcode_value):
            return False

        prev_opcode_value = self._opcode_value[:]
        next_operands = self._try_add_example(encoding_example, operands_examples)

        if next_operands is None: # match failed
            self._opcode_value = prev_opcode_value
            return False
        else:
            self._operands = next_operands
            self._num_examples += 1
            for all_op, op_ex in zip(self._all_operands, operands_examples):
                all_op.add(op_ex)
            return True

        
    def opcode(self):
        if self._opcode_value is None:
            return None

        const_bits_per_operand = self._const_operand_bits()

        operand_indices = set()
        for idx, op in enumerate(self._operands):
            const_bits = const_bits_per_operand[idx]
            if len(self._all_operands[idx]) > 1:
                for bit_idx, op_bit in enumerate(op):
                    if const_bits[bit_idx] == '-':
                        operand_indices.update(op_bit)

        return "".join(('?' if (idx in operand_indices and b != '-') else b)
                         for idx, b in enumerate(self._opcode_value))

    def _const_operand_bits(self):
        uniqe_bits_per_operands = [list(list(x)[0]) for x in self._all_operands]
        for op_idx, operand_examples in enumerate(self._all_operands):
            uniqe_bits = uniqe_bits_per_operands[op_idx]
            for example in operand_examples:
                for bit_idx, bit_value in enumerate(example):
                    if uniqe_bits[bit_idx] != bit_value:
                        uniqe_bits[bit_idx] = '-'

        # There might be operand bits that does not have a determined index yet.
        # There reason for this might be that the candidate indices all point to the
        # same bit value. If so, this phase replaces all those indices with a single
        # constant bit value.
        for idx, op in enumerate(self._operands):
            const_bits = uniqe_bits_per_operands[idx]
            for bit_idx, bit in enumerate(op):
                if const_bits[bit_idx] == '-':
                    bit_values = set(self._opcode_value[b] for b in bit)
                    if len(bit_values) == 1:
                        const_bits[bit_idx] = list(bit_values)[0]

        return uniqe_bits_per_operands


    def operands(self):
        const_bits_per_operand = self._const_operand_bits()
        operands = []
        for idx, op in enumerate(self._operands):
            const_bits = const_bits_per_operand[idx]
            if len(self._all_operands[idx]) == 1:
                operands.append(list(self._all_operands[idx])[0])
            else:
                ops = []
                for bit_idx, bit in enumerate(op):
                    if const_bits[bit_idx] != '-':
                        ops.append(const_bits[bit_idx])
                    else:
                        assert len(bit) == 1, bit
                        ops.append(list(bit)[0])
                operands.append(tuple(ops))
        return tuple(operands)


    def __repr__(self):
        opcode = self.opcode()
        const_bits_per_operand = self._const_operand_bits()

        operands = []
        for idx, op in enumerate(self._operands):
            if len(self._all_operands[idx]) == 1:
                operands.append('op%s=<%s>' % (idx, list(self._all_operands[idx])[0]))
            else:
                const_bits = const_bits_per_operand[idx]
                ops = []
                for bit_idx, bit in enumerate(op):
                    if const_bits[bit_idx] != '-' and False:
                        ops.append('=' + const_bits[bit_idx])
                    else:
                        ops.append("|".join(str(x) for x in bit))
                operands.append("op%s=[%s]" % (idx, ", ".join(ops)))

        uniqops = ",".join(str(len(x)) for x in self._all_operands)
        return "ExampleSet(num=%s, uniqops=%s, opcode(%s)=%s, %s)" % (self._num_examples,
                                                                      uniqops,
                                                                      len(opcode), 
                                                                      opcode,
                                                                      ", ".join(operands))


class EncodingFinder:
    def __init__(self):
        self._examples = []

    def add_example(self, encoding_example, operands_examples):
        for example in self._examples:
            matched = example.add_example(encoding_example, operands_examples)
            if matched:
                return

        example = ExampleSet(encoding_example, operands_examples)
        example.add_example(encoding_example, operands_examples)
        self._examples.append(example)

    def encoding_formats(self):
        return [(x.opcode(), x.operands()) for x in self._examples]

    def __repr__(self):
        return "{%s}" % ", ".join(repr(x) for x in self._examples)


def find_encoding(examples, failed_instantiations):
    """    
    examples: iterable of 2-tuples. Each 2-tuple is an example of encoding and
        values of the operands which was used for this example. The operand
        values are represented as a n-tuple with one element for each operand.
        [('0000', ('00', '00')),
         ('0001', ('00', '01')),
         ('0010', ('00', '10')),
         ('0011', ('00', '11')),
         ('0100', ('01', '00')),
         ('1000', ('10', '00')),
         ('1100', ('11', '00'))]
    """
    finder = EncodingFinder()
    for encoding_example, operands_examples in examples:
        if operands_examples in failed_instantiations:
            continue
        finder.add_example(encoding_example, operands_examples)

    return finder.encoding_formats()