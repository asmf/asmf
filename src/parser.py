__all__ = ["parse"]

from pyparsing import (Suppress,
                       Literal,
                       quotedString,
                       cppStyleComment,
                       nestedExpr)
 
top = Suppress(quotedString) | Literal("asmf") + nestedExpr()
top.ignore(cppStyleComment)

def parse(string):
    # Bunch of ugly code here to fix up the matches given by pyparsing. The uglyness
    # comes mostly from the fact that the pyparsing-expression above is not the
    # correct grammar for parsing function calls, thus gives false matches.
    pattern_start = 1 << 31
    pattern_end = 0
    for match, start_pos, end_pos in top.scanString(string):
        if len(match) == 0:
            continue
        pattern = ""
        for token in match[1]:
            if type(token) == str:
                if token[0] == '"' and token[-1] == '"': # string literal
                    pattern += token[1:-1]
                    idx = string.find(token, start_pos)
                    pattern_start = min(pattern_start, idx)
                    pattern_end = max(pattern_end, idx + len(token))

        yield pattern, (start_pos, end_pos), (pattern_start, pattern_end)

