#!/usr/bin/env python

from encoder import find_encoding
from code_generator import generate_assembler
from assembler import assemble
from instantiator import instantiate_wildcards
from parser import parse
import pickle
import re


###
### Helper functions.
###

def int2bin(value, num_bits):
    b = bin(value)[2:].replace("L", "")
    assert len(b) <= num_bits
    return "0" * (num_bits - len(b)) + b

def int2hex(value):
    return hex(value).replace('L', '')




INVALID_CHARS = re.compile("[^a-zA-Z0-9]")

def translate(pattern, defines):
    """
    Takes an assembly pattern instring an a dictionary describing how to
    instantiate the wildcards in the pattern and returns C code for encoding
    this pattern.
    """

    wildcard_instantiations = instantiate_wildcards(pattern, defines)
    assembled_instantiations, failed_instantiations = assemble(pattern, wildcard_instantiations)
    
    encoding = find_encoding(assembled_instantiations, failed_instantiations)
    funcname = "__asmf_" + INVALID_CHARS.sub('_', pattern)

    funccode = generate_assembler(funcname, encoding, failed_instantiations)
    funccode = "// %s\n%s"  % (pattern, funccode)
    return funcname, funccode
#    except:
#        print "Failed to process '%s'." % pattern
#        exit(-1)


X64_DEFINES = {'r': "%rax %rcx %rdx %rbx %rsp %rbp %rsi %rdi %r8 %r9 %r10 %r11 %r12 %r13 %r14 %r15".split(),
               'e': "%eax %ecx %edx %ebx %esp %ebp %esi %edi".split()
              }


def read_cache():
    try:
        with open(".asmf", 'r') as f:
            return pickle.Unpickler(f).load()
    except:
        return {}


def write_cache(cache):
    try:
        with open(".asmf", 'w') as f:
            pickle.Pickler(f).dump(cache)
    except:
        pass

CACHE = None
def preprocess(filename, input_file, output_file):
    global CACHE
    if CACHE is None:
        CACHE = read_cache()
    last_end = 0
    result = []
    content = input_file.read()
    funcs = set()
    for pattern, (call_begin, call_end), (pattern_begin, pattern_end) in parse(content):
        if pattern not in CACHE:
            funcname, funccode = translate(pattern, X64_DEFINES)
            CACHE[pattern] = funcname, funccode
        else:
            funcname, funccode = CACHE[pattern]
        funcs.add(funccode)
        result.append(content[last_end:call_begin])
        result.append(funcname + "(")
        comma_end = content.find(",", pattern_end) + 1
        result.append(content[comma_end:call_end])
        last_end = call_end

    output_file.write("#include <stdlib.h>\n")
    output_file.write("#include <assert.h>\n")
    output_file.write("\n".join(funcs))
    output_file.write('#1 "%s"\n' % filename)
    output_file.write("".join(result))
    output_file.write(content[last_end:])

    write_cache(CACHE)

###
### Command line argument handling.
###


def main():
    import argparse

    parser = argparse.ArgumentParser(description='asmf preprocessor',
                                     epilog="See <homepage> for more information.")
    parser.add_argument('infile', metavar='INFILE',
                        type=argparse.FileType('r'),
                        help='file to preprocess')
    parser.add_argument('-o', dest='outfile',
                        type=argparse.FileType('w'),
                        help='output file')

    args = parser.parse_args()
    preprocess(args.infile.name, args.infile, args.outfile)

if __name__ == '__main__':
    main()