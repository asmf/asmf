import re
import subprocess

__all__ = ['assemble']


WILDCARD_PATTERN = re.compile(r'%((\(.*?\))|(([-0-9]+)i)|[a-zA-Z]\b)')

OBJDUMP_LINE_RE = re.compile(r'^\s+[0-9a-zA-Z]+:\t(([0-9a-fA-F]{2} )+)\s*\t')


def run_objdump(filename, instantiations):
    """
    Use objdump to get the binary representation of the code in 'filename'.
    The file is assumed to be the assembly of the instantiations in
    'instantiations'.
    """
    objdump = subprocess.Popen(["objdump", filename + ".o", "--wide", "-d"], stdout=subprocess.PIPE)

    # Get the output from objdump and add a trailing text sequence that is later used
    # for finding the end of each pattern instantation. Doing it this way simplifies
    # the parsing below.
    output = objdump.communicate()[0] + "\n<instantiation"
    
    code_for_instantiation = []
    offset = 0
    for idx, instantiation in enumerate(instantiations):
        # Get the beginning and the end of the idx:th instantiation.
        start = output.find("\n", output.find("<instantiation%s>:" % idx, offset)) + 1
        end = output.rfind("\n", start, output.find("<instantiation", start))

        # Concatinate the binary code for the instantiation.
        hexasm = []
        for line in (l for l in output[start:end].split('\n') if l.strip()):
            m = OBJDUMP_LINE_RE.match(line)
            hexasm.append(m.group(1).replace(" ", ""))

        hexasm_str = "".join(hexasm)

        num_bits = len(hexasm_str) * 4
        binasm = bin(int(hexasm_str, 16))[2:] # Convert to string of binary digits.
        padded_binasm = "0" * (num_bits - len(binasm)) + binasm
        code_for_instantiation.append((padded_binasm, tuple(x.bin_value for x in instantiation)))

        offset = end

    return code_for_instantiation


def pattern_to_fmtstr(pattern):
    """
    Takes a pattern, e.g., 'mov %r, 0' and creates a python format string,
    e.g., 'mov {0}, 0'.
    """
    class Replacer:
        def __init__(self):
            self._count = -1
        def __call__(self, match):
            self._count += 1
            return "{%s}" % self._count
    return WILDCARD_PATTERN.sub(Replacer(), pattern)

ASM_ERROR_RE = re.compile(r'\.asm:([0-9]+): Error:')

def do_run_assembler(filename, pattern_fmt, instantiations, failed_instantiations):
    with open(filename + ".asm", 'w') as asm_file:
        for idx, instantiation in enumerate(instantiations):
            asm_file.write('instantiation%s:\n' % idx)
            values = tuple(ins.asm_value for ins in instantiation)
            if idx in failed_instantiations:
                asm_file.write('nop\n')
            else:
                asm_file.write(pattern_fmt.format(*values) + '\n')
        asm_file.write('instantiation%s:\n' % (idx + 1))

    try:
        subprocess.check_output(["as", "--msyntax=intel", filename + ".asm" ,"-o", filename + ".o"],
                                stderr=subprocess.STDOUT)
        return True
    except subprocess.CalledProcessError as ex:
        message = ex.output
    added_something = False
    for match in ASM_ERROR_RE.finditer(message):
        added_something = True
        failed_instantiations.add(int(match.group(1)) / 2 - 1)
    assert added_something, message
    return False

def run_assembler(pattern, instantiations):
    """
    Assemble the pattern with wildcard nstantiations in 'instantiations'.
    Return the output .asm file
    """
    import os
    import time
    # Generate a temporary filename for the assembled binary file.
    filename = "/tmp/asmf-%s-%s" % (os.getpid(), time.time())
    pattern_fmt = pattern_to_fmtstr(pattern)
    failed_instantiations = set()

    done = False
    while not done:
        done = do_run_assembler(filename, pattern_fmt, instantiations, failed_instantiations)

    return filename, failed_instantiations

def assemble(pattern, instantiations):
    """
    pattern: the assembler pattern, e.g., 'mov %r, 0'.
    instantiations: list of tuples. Each tuple holds WildcardInstantiation
        that represents the concrete value of each wildcard in the pattern.
    """
    asm_filename, failed_instantiations = run_assembler(pattern, instantiations)
    assembled_instantiations = run_objdump(asm_filename, instantiations)

    failed_inst_bin = set(tuple(ins.bin_value for ins in instantiations[idx]) for idx in failed_instantiations)
    return assembled_instantiations, failed_inst_bin

