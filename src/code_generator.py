import collections

def shift_str(shift_amount):
    if shift_amount > 0:
        return " >> %s" % shift_amount
    elif shift_amount < 0:
        return " << %s" % -shift_amount
    return ""

def bytehex(value):
    h = hex(value)
    if len(h) == 3: # One digi hex: 0x_
        return "0x0" + h[-1]
    return h

def byteify(bit_list):
    """
    Takes a list and return a list of 8-tuples. Example:
        byteify([0, 1, 2, ..., 15]) == [[0, 1, ...7], [8, 9, ..., 15]]
    """
    byte_list = []
    current_byte = []
    for bit in bit_list:
        current_byte.append(bit)

        if len(current_byte) == 8:
            byte_list.append(tuple(current_byte))
            current_byte = []
    assert len(current_byte) == 0
    return byte_list

def get_opcode_bytes(opcode_bitstr):
    """
    Given a bitstring representing how an instruction is encoded, e.g.,
    "0100100-10010---", returns a list of hex strings representing the
    bytes of the instructions. The bytes are those bits that represent
    the opcode (that is, bits that do not depend on the operand values).
    """
    opcode_bytes = []
    for byte in byteify(opcode_bitstr):
        opcode_int = sum(int(bit.replace('-', '0')) << idx
                         for idx, bit in enumerate(reversed(byte)))
        opcode_bytes.append(bytehex(opcode_int))
    return opcode_bytes
    

def indices_of(elem, lst):
    idxs = []
    try:
        idx = -1
        while True:
            idx = lst.index(elem, idx + 1)
            idxs.append(idx)
    except ValueError:
        return idxs

def get_operands_assumptions(operands):
    """
    Get a list of the assumption that is made on each operand. An assumption
    is value of an operand that is not encoded directly into the instruction
    word.
    """
    assumption_checks = []
    for op_idx, operand in enumerate(operands):
        if type(operand) == str:
            mask = int(operand.replace('0', '1'), 2)
            value = int(operand, 2)

            mask = hex(int(operand.replace('0', '1').replace('-', '0'), 2))
            assumption_checks.append("((op%s & %s) == %s)" % (op_idx, mask, value))
        else:
            assert type(operand) == tuple
            mask = 0
            value = 0
            for bit_idx, bit_value in enumerate(reversed(operand)):
                if type(bit_value) == str:
                    mask |= (1 << bit_idx)
                    value |= (int(bit_value) << bit_idx)
            if mask != 0:
                assumption_checks.append("((op%s & %s) == %s)" % (op_idx, hex(mask), hex(value)))

            # find bits that has to be equal in operand
            r_operand = list(reversed(operand))
            taken_indices = set()
            for idx, bit in enumerate(r_operand):
                if type(bit) == int and bit not in taken_indices:
                    taken_indices.add(bit)
                    idxs = indices_of(bit, r_operand)
                    if len(idxs) > 1:
                        mask =  hex(sum(1 << i for i in idxs))
                        expr = "(op%s & %s)" % (op_idx, mask)
                        assumption_checks.append("((%s == 0) || (%s == %s))" % (expr, expr, mask))

    result = None
    if len(assumption_checks) > 0:
        result = "(" + " && ".join(assumption_checks) + ")"
    return result


def get_operands_encodings(operands, opcode_bytes):
    """
    Returns a list where each element represents how to encode each 
    byte in the opcode. Each such byte is represented as an 8 element
    long list where each element represents how to encode each bit.
    Each such element is either None (bit is not encoded by an operand)
    or a 2-tuple (bit is encoded by an operand). The tuple is a pair of
    operand index and operand bit index.
    """
    operands_encodings = [[None for _ in range(8)] for _ in range(len(opcode_bytes))]
    for op_idx, operand in enumerate(operands):
        if type(operand) == tuple:
            for operand_bit_idx, opcode_bit_idx in enumerate(operand):
                if type(opcode_bit_idx) == int: # it's an offset
                    #assert operands_encodings[opcode_bit_idx // 8][opcode_bit_idx % 8] == None, operands_encodings
                    operands_encodings[opcode_bit_idx // 8][opcode_bit_idx % 8] = (op_idx, len(operand) - operand_bit_idx - 1)
                else:
                    assert type(opcode_bit_idx) == str # it's a concrete value
                    # Assumption is added in 'get_operands_assumptions'.
        else:
            assert type(operand) == str
    return operands_encodings


def get_encoding_bytes(opcode_bytes, operands_encodings):
    """
    Get a list of strings that represent how to encode each byte of the
    instruction word with regard to the operands of the instruction.
    """
    bytes_encoding = []
    for opcode_byte, operand_byte in zip(opcode_bytes, operands_encodings):
        assert type(opcode_byte) == str

        op_mask = collections.defaultdict(lambda: 0)
        op_shift_from = collections.defaultdict(lambda: 256) # Max shift == max operand size in bits.
        op_shift_to = collections.defaultdict(lambda: 0)
        for bit_idx, bit in enumerate(operand_byte):
            if bit is None:
                continue
            op_idx, op_bit_idx = bit
            op_mask[op_idx] |= (1 << op_bit_idx)
            op_shift_from[op_idx] = min(op_shift_from[op_idx], op_bit_idx)
            op_shift_to[op_idx] = max(op_shift_to[op_idx], bit_idx)
        
        byte_encoding = [opcode_byte]
        for op_idx in op_mask.iterkeys():
            mask = op_mask[op_idx]
            shift = shift_str(op_shift_from[op_idx] - 7 + op_shift_to[op_idx])
            code = "(op%s & %s)%s" % (op_idx, bytehex(mask), shift)
            if shift != '':
                code = "(%s)" % code
            byte_encoding.append(code)
        
        bytes_encoding.append(" | ".join(byte_encoding))
    return bytes_encoding


def generate_emitter_code(assumption_checks, encoding_bytes, code):
    prefix = " " * 4
    if assumption_checks is not None:
        code.append('    if %s {' % assumption_checks)
        prefix = " " * 8

    code.extend("%sbuf[%s] = %s;" % (prefix, idx, byte)
                for idx, byte in enumerate(encoding_bytes))
    code.append("%s*bufp += %s;" % (prefix, len(encoding_bytes)))
    code.append("%sreturn;" % prefix)

    if assumption_checks is not None:
        code.append('    }')

def get_illegal_args_checks(failed_instantiations):
    if len(failed_instantiations) == 0:
        return None
    const_bits_per_operand = [list(i) for i in list(failed_instantiations)[0]]
    unique_values_per_operand = [set() for _ in list(failed_instantiations)[0]]

    for instantiation in failed_instantiations:
        for op_idx, op_value in enumerate(instantiation):
            const_bits = const_bits_per_operand[op_idx]
            unique_values_per_operand[op_idx].add(op_value)
            for bit_idx, bit_value in enumerate(op_value):
                if const_bits[bit_idx] != bit_value:
                    const_bits[bit_idx] = '-'

    checks = []
    for op_idx, const_bits in enumerate(const_bits_per_operand):
        mask = 0
        value = 0
        for idx, bit in enumerate(reversed(const_bits)):
            if bit != '-':
                mask |= 1 << idx
                value |= int(bit) << idx
        if mask != 0:    
            checks.append("((op%s & %s) != %s)" % (op_idx, hex(mask), hex(value)))

#    print unique_values_per_operand
    assert len(checks) > 0
    return "(" + " && ".join(checks) + ")"

def generate_assembler(funcname, formats, failed_instantiations):
    """
    Generate a C function that assembles the instruction described by 'formats'.
    The argument 'formats' is the return value of encoding.find_encoding, which is
    a list the instruction format described by 2-tuples. Example:

                      opcode                     op0               op1
        formats = [('10010000',                 ('0000',          '0000')),
                   ('0100100-10010---',         ('0000',          (6, 12, 13, 14))),
                   ('01001-0-1000011111------', ((4, 17, 18, 19), (6, 20, 21, 22)))]
    
    An operand that is described as a string is an operand that has a hard-coded
    value for that particular instruction format. On the other hand, if the operand
    is described by a tuple of integers, then the integers say at which index in the
    instruction the operand's bit is encoded. For example the 6th bit of the instruction
    word encodes the 1st bit of op1.
    """

    encoding_bytes_per_format = []
    assumption_checks_per_format = []

    for opcode_bitstr, operands in formats:
        opcode_bytes = get_opcode_bytes(opcode_bitstr)
        
        operands_assumptions = get_operands_assumptions(operands)

        operands_encodings = get_operands_encodings(operands, opcode_bytes)
        encoding_bytes = get_encoding_bytes(opcode_bytes, operands_encodings)

        encoding_bytes_per_format.append(encoding_bytes)
        assumption_checks_per_format.append(operands_assumptions)

    illegal_args_checks = get_illegal_args_checks(failed_instantiations)

    num_ops = len(formats[0][1])
    args = ['unsigned long op%s' % i for i in range(num_ops)] + ['unsigned char** bufp']
    code = ['void %s(%s) {' % (funcname, ", ".join(args))]
    if illegal_args_checks:
        code.append('    assert(%s && "Illegal argument");' % illegal_args_checks)
    code.append('    unsigned char* buf = *bufp;')

    it = zip(assumption_checks_per_format, encoding_bytes_per_format)
    sorted_it = sorted(it, key=lambda x: -1 if x[0] is None else -len(x[0]))
    for assumption_checks, encoding_bytes in sorted_it:
        generate_emitter_code(assumption_checks, encoding_bytes, code)
    code.append("    abort();")
    code.append("}\n")
    return "\n".join(code)